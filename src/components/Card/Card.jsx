import React, { useState } from "react";
import "./Card.css";
import "react-circular-progressbar/dist/styles.css";
import { AnimateSharedLayout } from "framer-motion";
import ExpandedCard from "./ExpandedCard";
import CompactCard from "./CompactCard";

const Card = (props) => {
  const [expanded, setExpanded] = useState(false);
  return (
    <AnimateSharedLayout>
      {expanded ? (
        <ExpandedCard param={props} setExpanded={() => setExpanded(false)} />
      ) : (
        <CompactCard param={props} setExpanded={() => setExpanded(true)} />
      )}
    </AnimateSharedLayout>
  );
};

export default Card;
